classdef BinaryDatasetGenerator
    properties
        noFeatures;
        stateDistribution; %Distribution over the leaf hypercubes
        positiveDistribution; %Positive mass per leaf
        noStates;  %The number of leaves in the tree
    end
    methods
        function obj = BinaryDatasetGenerator(noF, sD, pD)
            obj.noFeatures = noF;
            obj.stateDistribution = sD;
            obj.positiveDistribution = pD;
            obj.noStates = 2^noF;
        end
        function [X, L] = getNextPoint(obj)
            selectedState = rand_sample(obj.stateDistribution);
            X = getPoint(obj, selectedState);
            L = getLabel(obj, selectedState);
        end
        function label = getLabel(obj, state)
            x = rand;
            p = obj.positiveDistribution(state);
            if x < p
                label = 1;
            else
                label = -1;
            end
        end
        function f = getPoint(obj, index)
            X = de2bi(index-1);
            f = rand(1, obj.noFeatures)/2;
            if size(X, 2) ~= obj.noFeatures
                diff = obj.noFeatures - size(X, 2);
                X = [X zeros(1, diff)];
            end
            f = X*0.5 + f;
        end
    end
end