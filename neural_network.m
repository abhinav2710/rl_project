% P = {[1;2] [2;1] [2;3] [3;1]};
% T = {4 5 7 7};
% net = linearlayer(0,0);
% net = configure(net,P,T);
% net.IW{1,1} = [0 0];
% net.b{1} = 0;
% 
% 
% net.inputWeights{1,1}.learnParam.lr = 0.25;
% net.biases{1,1}.learnParam.lr = 0.1;
% % 
% 
% net = network( ...
% 2, ... % numInputs, number of inputs,
% 2, ... % numLayers, number of layers
% [1; 0], ... % biasConnect, numLayers-by-1 Boolean vector,
% [1; 0], ... % inputConnect, numLayers-by-numInputs Boolean matrix,
% [0 0; 1 0], ... % layerConnect, numLayers-by-numLayers Boolean matrix
% [0 1] ... % outputConnect, 1-by-numLayers Boolean vector
% );
% 
% P = {[1;3],[2;2],[2;4],[1;2],[0;3]};
% T = {4 4 6 3 3};
% [net,a,e,pf] = adapt(net,P,T);
% e
%[x, t] = simplefit_dataset;
data = csvread('airfoil_self_noise.csv');
train_x = data(1:1000, 1:5)';
train_t = data(1:1000, 6)';
test_x= data(1001:end, 1:5)';
test_t = data(1001:end, 6)';

% Added the train Fcn thingy because of http://in.mathworks.com/matlabcentral/newsreader/view_thread/311629
hidden_layers = [10];
net = feedforwardnet(hidden_layers);

net = train(net, train_x, train_t);
y = net(test_x);
batch_perf = perform(net,y,test_t);
online_points = test_x(:, 1:300);
online_target = test_t(:, 1:300);

rest_test_x = test_x(:, 301:end);
rest_test_t = test_t(:, 301:end);

for i = 1: size(online_points, 2)
    net = adapt(net, online_points(:, i), online_target(:, i));
    predicted = net(rest_test_x);
    p = perform(net, predicted, rest_test_x)
    
end



%% Online Approach 1
% online_net = feedforwardnet(hidden_layers);
% % online_net.trainFcn='traingdm';
% % online_net.inputWeights{1,1}.learnFcn = 'learngdm';
% % online_net.layerWeights{1,1}.learnFcn = 'learngdm'; 
% % online_net.biases{1}.learnFcn = 'learngdm'; 
% 
% permutation = randperm(size(train_x, 2));
% rx = train_x(:, permutation);
% rt = train_t(:, permutation);
% online_performance_plot = [];
% set_data={};
% set_label={};
% for i = 1:size(rx, 2)
%     set_data{i} = rx(:, i);
%     set_label{i} = rt(:, i);    
% end
% [online_net, a, e, pf] = adapt(online_net, set_data, set_label);
% Y1 = online_net(test_x);
% online_net1 = online_net;
% online1_perform = perform(online_net, Y1, test_t);
% 
% 
% %% Online Approach 2
% 
% online_net = feedforwardnet(hidden_layers);
% % online_net.trainFcn='traingdm';
% % online_net.inputWeights{1,1}.learnFcn = 'learngdm';
% % online_net.layerWeights{1,1}.learnFcn = 'learngdm'; 
% % online_net.biases{1}.learnFcn = 'learngdm'; 
% %online_net = configure(online_net, test_x(:, 1), train_t(:, 1));
% performances = [];
% for i=1:size(rx, 2)
%     point = {};
%     point{1} = train_x(: , i);
%     point{2} = train_x(: , i);
%     target = {};
%     target{1} = train_t(:, i);
%     target{2} = train_t(:, i);
%     [online_net, a, e, pf] = adapt(online_net, point, target);
%     Y = online_net(test_x);
%     performances = [performances; perform(online_net, Y, test_t)];
% end
% Y2 = online_net(test_x);
% online2_perform = perform(online_net, Y2, test_t);
% plot(performances);
% % Notice that all values of Y2 are same as 'a' ie, last learnt ouput
% online_net2 = online_net;
% 
% %% Attempt 3!
% % Run This alone
% % Change batch size and notice the size of the IW layer. For 1, 
% % size is supposed to be hidden_layer_size * 5 (no_features)
% % try batch_size 10, 50 , 100
% 
% % Hypothesis - There is a minimum batch size for the network to fully
% % learnt
% % To check if I reduce the network size, will that reduce the number of
% % points required? 
% new_data = {};
% new_label = {};
% batch_size = 1000;
% 
% hidden_node_size = 10;
% for i = 1:batch_size
%     new_data{i} = train_x(:, i);
%     new_label{i} = train_t(:, i);
% end
% online_net3 = fitnet(hidden_node_size);
% online_net3 = train(online_net3, new_data, new_label);
% 
% online_net3.IW{1, 1}
% % Notice net.IW{1,1}
% % online_net1.IW{1, 1}
% % online_net2.IW{1, 1}
% % online_net3.IW{1, 1} - Why is the size weird? :-O
% 
