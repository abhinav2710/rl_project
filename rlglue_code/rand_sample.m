function sample = rand_sample(distribution)
    B = cumsum(distribution);
    x = rand(1);
    lower_bounds = (B > x);
    [~, sample] = max(lower_bounds);
end
