%Test Script
noPoints = 1000;
X = zeros(noPoints, 2);
L = zeros(noPoints, 1);
nFeatures = 2;
sD = [0.20 0.20 0.40 0.20];%ones(1, 4)/4;
pD = [1.0; 1.0; 0.01; 1.0];%[0.9; 0.9; 0.01; 0.9];
dataSet = BinaryDatasetGenerator(nFeatures, sD, pD);
for i = 1:noPoints
    [X(i, :), L(i, :)] = dataSet.getNextPoint();   
end
close all;
figure;
hold all;
for i = 1:noPoints
    if L(i) == 1
        plot(X(i, 1), X(i, 2), 'r*');
    else
        plot(X(i, 1), X(i, 2), 'b-');
    end            
end