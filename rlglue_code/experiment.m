function experiment()

    RL_init();
    fprintf(1, 'Starting the experiment\n');
	
	RL_agent_message('debug_file trace.txt');

    no_episodes = 100;
    for i = 1:no_episodes
        fprintf(1, sprintf('Episode Number %d\n', i));
        RL_episode(0);
    end
    RL_cleanup();
end

function[theMean,theStdDev]= evaluate_agent()
    n=10;
    sum=0;
    sum_of_squares=0;
 
    RL_agent_message('freeze learning');
    for i=1:n
        % We use a cutoff here in case the policy is bad and will never end
        % an episode
        RL_episode(5000);
        this_return=RL_return();
        sum=sum+this_return;
        sum_of_squares=sum_of_squares+this_return*this_return;
    end
 
    theMean=sum/n;
    variance = (sum_of_squares - n*theMean*theMean)/(n - 1.0);
    theStdDev=sqrt(variance);

    RL_agent_message('unfreeze learning');
end
 

%
%	This function will freeze the agent's policy and test it after every 25 episodes.
%
function offline_demo()

    statistics=[];
    
	[theMean,theStdDev]=evaluate_agent();
	print_score(0,theMean,theStdDev);
    
    %This is probably a bad way to do this, but
        %First column is the episode number
        %Second column is the mean at that evaluation
        %Third is the std deviation at that evaluation
    %Statistics are estimated from not many samples.  Beware!
      
    statistics=[statistics; 0,theMean,theStdDev];

    
    for i=1:20
        for j=1:25
			RL_episode(0);
        end
    	[theMean,theStdDev]=evaluate_agent();
		print_score(i*25,theMean,theStdDev);
        statistics=[statistics; i*25,theMean,theStdDev];
    end
	
    errorbar(statistics(:,1),statistics(:,2),statistics(:,3))

end


function print_score(afterEpisodes, theMean, theStdDev)
    fprintf(1,'%d\t\t%.2f\t\t%.2f\n', afterEpisodes,theMean, theStdDev);
end



