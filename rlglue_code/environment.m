
function theEnvironment=environment()
%Assign members of the returning struct to be function pointers
	theEnvironment.env_init=@my_init;
	theEnvironment.env_start=@my_start;
	theEnvironment.env_step=@my_step;
	theEnvironment.env_cleanup=@my_cleanup;
	theEnvironment.env_message=@my_message;
end


function taskSpecString=my_init()
    global my_struct;
    
    my_struct.noFeatures = 2;
    my_struct.stateDistribution = [0.20 0.20 0.40 0.20];
    my_struct.positiveDistribution = [1.0; 1.0; 0.01; 1.0];
    my_struct.dataSet = BinaryDatasetGenerator(my_struct.noFeatures, my_struct.stateDistribution, my_struct.positiveDistribution);
    % Not Required
%     my_struct.currState = zeros(2*my_struct.noFeatures, 1);
%     for i = 1 : my_struct.currState
%         if mod(i, 2) == 0
%             my_struct.currState(i, 1) = 1;
%         end
%     end
    theTaskSpecObject = org.rlcommunity.rlglue.codec.taskspec.TaskSpecVRLGLUE3();
    theTaskSpecObject.setEpisodic();
    theTaskSpecObject.setDiscountFactor(0.99);

    % Missed Observation Range
    observationRange=org.rlcommunity.rlglue.codec.taskspec.ranges.IntRange(0, 1);
    theTaskSpecObject.addDiscreteObservation(observationRange);

    actionRange=org.rlcommunity.rlglue.codec.taskspec.ranges.IntRange(0,my_struct.noFeatures+2);
    theTaskSpecObject.addDiscreteAction(actionRange);

    %Specify the reward range [-100,10]
    rewardRange=org.rlcommunity.rlglue.codec.taskspec.ranges.DoubleRange(-100,100);
    theTaskSpecObject.setRewardRange(rewardRange);
    taskSpecString = theTaskSpecObject.toTaskSpec();
end

%This is what will be called for env_start
function theObservation=my_start()
    global my_struct;
    [my_struct.currentPoint, my_struct.currentLabel]= my_struct.dataSet.getNextPoint();   
    theObservation = org.rlcommunity.rlglue.codec.types.Observation(1, 0, 0);
    theObservation.setInt(0, -1);
end



%This is what will be called for env_step
function rewardObservation=my_step(thisAction)
    global my_struct;

%Make sure the action is valid
    assert (thisAction.getNumInts() == 1,'Expecting a 1-dimensional integer action.');
    assert (thisAction.getInt(0) >= 0,'Action too small, should be in [0,4].');
    assert (thisAction.getInt(0) < my_struct.noFeatures +2 ,'Action too large.');
    R = 5;
    isTerminalInt=  0;
    newStateInt = 0 ;
    action = thisAction.getInt(0);
    if action < my_struct.noFeatures
        newReward = -1;
        if my_struct.currentPoint(action+1) > 0.5
            newStateInt = 1;
        end
    elseif action == my_struct.noFeatures
       	isTerminalInt = 1;
	    if my_struct.currentLabel == 1
		    newReward = +R;
	    else
		    newReward = -R;
	    end
    elseif action == my_struct.noFeatures + 1
      	isTerminalInt = 1;
	    if my_struct.currentLabel == 1
		    newReward = -R;
	    else
		    newReward = +R;
        end
    else
    end  
    %Fix the observation
	theObservation = org.rlcommunity.rlglue.codec.types.Observation();
	theObservation.intArray=[newStateInt];
	rewardObservation=org.rlcommunity.rlglue.codec.types.Reward_observation_terminal(newReward,theObservation,isTerminalInt);
end


%This is what will be called for env_message
function returnMessage=my_message(theMessageJavaObject)
    global my_struct;
%Java strings are objects, and we want a Matlab string
    inMessage=char(theMessageJavaObject);    
    returnMessage='SamplesmyEnvironment(Matlab) does not respond to that message.';
end

function my_cleanup()
	global my_struct;
    %Rewrite this
	my_struct=rmfield(my_struct,'fixedStartState');
	my_struct=rmfield(my_struct,'stateDistribution');
	my_struct=rmfield(my_struct,'positiveDistribution');
	my_struct=rmfield(my_struct,'dataset');
    clear my_struct;
end


%
%
%Utility functions below
%
