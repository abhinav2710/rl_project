
function runAllTogether()
    !/usr/bin/rl_glue&
    theAgent=agent();
    theEnvironment=environment();
    theExperimentFunc=@experiment;  
    
    togetherStruct.agent=theAgent;
    togetherStruct.environment=theEnvironment;
    togetherStruct.experiment=theExperimentFunc;
    
    runRLGlueMultiExperiment(togetherStruct);
    
end