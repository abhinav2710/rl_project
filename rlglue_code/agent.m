function theAgent = agent()
    theAgent.agent_init=@my_init;
    theAgent.agent_start=@my_start;
    theAgent.agent_step=@my_step;
    theAgent.agent_end=@my_end;
    theAgent.agent_cleanup=@my_cleanup;
    theAgent.agent_message=@my_message;
end

function my_init(taskSpecJavaString)
	
	global agent_vars;
    agent_vars.policyFrozen=false;
    agent_vars.exploringFrozen=false;
    agent_vars.epsilon=.1;
    agent_vars.stepsize=@get_alpha;
    agent_vars.traceFileID = -1;
    
    theTaskSpec = org.rlcommunity.rlglue.codec.taskspec.TaskSpec(taskSpecJavaString);

    agent_vars.noFeatures = 2;%theTaskSpec.getNumDiscreteObsDims();
    agent_vars.numStates = theTaskSpec.getDiscreteObservationRange(0).getMax() + 1;
    
    assert (theTaskSpec.getNumDiscreteActionDims() == 1);
    assert (theTaskSpec.getNumContinuousActionDims() == 0);
    assert (~theTaskSpec.getDiscreteActionRange(0).hasSpecialMinStatus());
    assert (~theTaskSpec.getDiscreteActionRange(0).hasSpecialMaxStatus());
    agent_vars.numActions = theTaskSpec.getDiscreteActionRange(0).getMax();
    
    agent_vars.gamma = theTaskSpec.getDiscountFactor();
    agent_vars.accuracy = [];    
    agent_vars.return = [];
    agent_vars.train_set = [];
    agent_vars.hiddenNodes = 2;
    agent_vars.train_target = [];
    agent_vars.neural_net = feedforwardnet(agent_vars.hiddenNodes);     
    agent_vars.dist_set = [];
    agent_vars.dist_target = [];
    agent_vars.dist_net = feedforwardnet(agent_vars.hiddenNodes); 

    % Configure the network
    %agent_vars.neural_net = configure(agent_vars.neural_net, zeros(4+2+2, 1), 0);
    agent_vars.trained = 0;
    agent_vars.dist_trained = 0;
    agent_vars.neural_net.trainParam.showWindow = false;
    agent_vars.dist_net.trainParam.showWindow = false;
    agent_vars.buff = 0;
end    

%
% Choose an action e-greedily from the value function and store the action
% and observation.
function theAction=my_start(theObservation)
    global agent_vars;
    agent_vars.currReturn = 0.0;
    agent_vars.currGamma = 1.0;
    % Resetting Variables
    log('------------------------------------------------------------------\n');
    log('Starting a new Episode\n');
    agent_vars.currState = zeros(2*agent_vars.noFeatures, 1);
    %fprintf(1, 'Number of Features %d\n', agent_vars.noFeatures);
    for i = 1 : size(agent_vars.currState, 1)
        %TODO: Is this correct - ? Will yeild an encoding 0 1 0 1 ..... 0 1
        %right?
        if mod(i, 2) == 0
            agent_vars.currState(i, 1) = 1;
        end
    end
    
    
    % Take the action
    newActionInt = egreedy();
    log(sprintf('Action Selected %d\n', newActionInt));
    % TODO: Have to check this part
    theAction = org.rlcommunity.rlglue.codec.types.Action(1, 0, 0);
    theAction.setInt(0,newActionInt);    
    agent_vars.lastAction=theAction.duplicate();
    agent_vars.lastObservation = theObservation.duplicate();
end

% Choose an action e-greedily from the value function and store the action
% and observation.  Update the valueFunction entry for the last
% state,action pair.
function theAction=my_step(theReward, theObservation)
        global agent_vars;        
        % Misnomer, doesn't give me the state, but the response to the
        % action taken in the last step.
        newStateInt = theObservation.getInt(0);  
        log(sprintf('\t Response from Environment %d, with reward of %d\n', newStateInt, theReward));
        %TODO: fix indexing
        lastActionInt = agent_vars.lastAction.getInt(0);
        agent_vars.currReturn = agent_vars.currReturn  + agent_vars.currGamma*theReward;
        agent_vars.currGamma = agent_vars.currGamma * agent_vars.gamma;
        
        % TODO UPDATE Q-learning
        %Q_sa = agent_vars.valueFunction(lastActionInt,lastStateInt);
        %Q_sprime_aprime = agent_vars.valueFunction(newActionInt,newStateInt);
        %new_Q_sa = Q_sa + agent_vars.stepsize * (theReward + agent_vars.gamma * Q_sprime_aprime - Q_sa);        
        
        % Have to use old state here, so updateState after this
        Q_sa = get_value(agent_vars.currState, lastActionInt);
 
        
        
        %Update current state
        oldStateFeature = agent_vars.currState;
        updateState(newStateInt, lastActionInt);
        % This should be called in new state!
        newActionInt = egreedy();
        log(sprintf('Action taken %d\n', newActionInt));
        % Q learning
        %Q_sprime_aprime = getMaxQ([agent_vars.currState]);
        % Sarsa
        Q_sprime_aprime  = get_value(agent_vars.currState, newActionInt);
        %new_Q_sa = Q_sa + agent_vars.stepsize() * (theReward + agent_vars.gamma * Q_sprime_aprime - Q_sa);
        nextMaxQ = getMaxQ(agent_vars.currState);
        new_Q_sa = Q_sa + agent_vars.stepsize() * (theReward + agent_vars.gamma * nextMaxQ - Q_sa);
        log(sprintf('Updating %f with %f\n',  Q_sa, agent_vars.gamma *nextMaxQ));

        %Only update the value function if the policy is not frozen
        if ~agent_vars.policyFrozen
            % Retraining the neural network
            log('Training the neural Network\n');

            agent_vars.train_set = [agent_vars.train_set [stateEncoding(oldStateFeature); oneHotCoding(lastActionInt, agent_vars.numActions)]];
            agent_vars.train_target = [ agent_vars.train_target new_Q_sa];

            agent_vars.neural_net = feedforwardnet(agent_vars.hiddenNodes); 
            agent_vars.neural_net.trainParam.showWindow = false;
            agent_vars.neural_net=train(agent_vars.neural_net, agent_vars.train_set, agent_vars.train_target);
            agent_vars.trained = 1;
        end      

        %Creating the action a different way to showcase variety */
        theAction = org.rlcommunity.rlglue.codec.types.Action();
        theAction.intArray=[newActionInt];
        agent_vars.lastAction=theAction.duplicate();
        agent_vars.lastObservation = theObservation.duplicate();
end


% The episode is over, learn from the last reward that was received.
function my_end(theReward)
    global agent_vars;        

    agent_vars.currReturn = agent_vars.currReturn  + agent_vars.currGamma*theReward;
    agent_vars.return = [ agent_vars.return agent_vars.currReturn ];
    agent_vars.accuracy = [ agent_vars.accuracy theReward];
    
    log(sprintf('\t Terminal State with reward %d\n', theReward));
    lastStateInt = agent_vars.lastObservation.getInt(0);
    lastActionInt = agent_vars.lastAction.getInt(0);
    
     currState = agent_vars.currState;
    if currState(1) == 0 && currState(2) == 0.5 && currState(3) == 0.5 && currState(4) == 1.0
        if lastActionInt == agent_vars.numActions-1
            agent_vars.buff = agent_vars.buff + theReward;
        else
            agent_vars.buff = agent_vars.buff - theReward;
        end
        log(sprintf('\t Updating buff\n'));
    end
    display(agent_vars.buff);
    
    
    %Only update the value function if the policy is not frozen
    Q_sa = get_value(agent_vars.currState, lastActionInt);
    oldStateFeature = agent_vars.currState;
    %updateState(lastStateInt, lastActionInt);
    %Q_sprime_aprime = getMaxQ([agent_vars.currState]);
    new_Q_sa = Q_sa + agent_vars.stepsize() * (theReward - Q_sa);
        
    updateState(lastStateInt, lastActionInt);

    if ~agent_vars.policyFrozen
        
        agent_vars.dist_set = [agent_vars.dist_set [stateEncoding(oldStateFeature)]];
        if lastActionInt == agent_vars.numActions - 1
            new_Q_sa = -new_Q_sa;
        end
        agent_vars.dist_target = [ agent_vars.dist_target new_Q_sa];
         csvwrite('train_x.csv',agent_vars.dist_set);
         csvwrite('train_t.csv',agent_vars.dist_target);
        agent_vars.dist_net = feedforwardnet(agent_vars.hiddenNodes); 
        agent_vars.dist_net.trainParam.showWindow = false;
        agent_vars.dist_net=train(agent_vars.dist_net, agent_vars.dist_set, agent_vars.dist_target);
        agent_vars.dist_trained = 1;
    end
   printQ();
end

function returnMessage=my_message(theMessageJavaObject)
    global agent_vars;
    %Java strings are objects, and we want a Matlab string
    inMessage=char(theMessageJavaObject);
    if strcmp(inMessage,'debug_file')
        [~,remainder]=strtok(inMessage);
        fileName=strtok(remainder);
        agent_vars.traceFileID = fopen(fileName);
        fprintf(1, 'Opening Trace File\n');
		returnMessage='message understood, File read';
        return;
    end
    returnMessage='Unknown Message\n';
end



% Selects a random action with probability 1-sarsa_epsilon,
% and the action with the highest value otherwise.  This is a
% quick'n'dirty implementation, it does not do tie-breaking.
function theActionInt=egreedy()
    global agent_vars;
    if ~agent_vars.exploringFrozen
        if rand() <= agent_vars.epsilon
            % Sampling from actions not taken before
            % -1 to account for matlab indexing.
            allowedActions = [];
            for i=1:agent_vars.numActions
                if isActionAllowed(agent_vars.currState, i-1)
                    allowedActions = [allowedActions i];                 
                end
            end
            theActionInt=datasample(allowedActions, 1) - 1;
            return;
        end
    end

    %otherwise choose the greedy action
    %Fix this
    %[~,theActionInt]=max(agent_vars.valueFunction(:,theStateInt));
    [~,theActionInt]=getMaxQ(agent_vars.currState);
    %theActionInt=theActionInt-1;
end

function [ret] = get_value(currState, actionInt)
    global agent_vars;
    currState = stateEncoding(currState);
    if actionInt < agent_vars.numActions - 2
        if agent_vars.trained == 1
            ret = agent_vars.neural_net([currState; oneHotCoding(actionInt, agent_vars.numActions-2)]);
        else
            ret = 0;
        end
   elseif actionInt == agent_vars.numActions - 2
       if agent_vars.dist_trained == 1
            ret = agent_vars.dist_net([currState]);
       else
            ret = 0;
       end
    else
        if agent_vars.dist_trained == 1
            ret = -agent_vars.dist_net([currState]);
        else
            ret = 0;
        end
    end
end


function [ret] = isActionAllowed(currState, actionInt)
    global agent_vars;
    if actionInt < agent_vars.numActions - 2
        ret = not(currState(2*actionInt+2) == 0.5 || currState(2*actionInt+1) == 0.5);    
    else
        ret = true;
    end
end

function updateState(observation, LastActionInt)
    global agent_vars;
    %+1
    %if x >0.5
    %0 otherwise
    %Las
    if observation == 1
        agent_vars.currState(2*(LastActionInt)+1) = 0.5;
        % Should be 2*LastActionInt + 1  For the correct index, extra +1
        % accounting for matlab indexing
    else
        agent_vars.currState(2*(LastActionInt)+2) =  0.5;
        % Extra +1 accounting for matlab indexing
    end
end



function [val, ret, Q_values] =  getMaxQ(currState)
    global agent_vars;
    Q_values = zeros(1, agent_vars.numActions);
    for i = 1:agent_vars.numActions
        if isActionAllowed(currState, i-1)
            Q_values(1, i) = get_value(currState, i-1);
       else
           Q_values(1, i) = -inf;
       end
    end
    [val, ret] = max(Q_values);
    ret = ret-1;
    
end

function loadValueFunction(fileName)
global agent_vars;
    loadedStruct=load(fileName,'-mat');
    
    agent_vars.valueFunction=loadedStruct.valueFunction;
end

function saveValueFunction(fileName)
global agent_vars;
%I know this looks weird, trying to make:
% >>save fileName -mat -struct 'agent_vars' 'valueFunction'

    theSaveCommand=sprintf('save %s -mat -struct ''agent_vars'' ''valueFunction''',fileName);
    eval(theSaveCommand);
end

function encoding = oneHotCoding(n, T)
    encoding = zeros(T, 1);
    encoding(n+1, 1) = 1;    
    % +1 Accounting for matlab indexing
end


function encoding = stateEncoding(currState)
global agent_vars;
    encoding = zeros((agent_vars.numActions-2)*3, 1);
    for i = 1:(agent_vars.numActions-2)
        if currState(2*i-1) == 0.0 && currState(2*i) == 1.0
            encoding(3*i -2) = 1;
        elseif currState(2*i-1) == 0.0 && currState(2*i) == 0.5
            encoding(3*i -1) = 1;
        elseif currState(2*i-1) == 0.5 && currState(2*i) == 1.0
            encoding(3*i) = 1;
        end
    end   
    % +1 Accounting for matlab indexing
end


function printQ()
    global agent_vars;
    start_state = zeros(4, 1);
    all_states = zeros(4, 16);
    for i = 1 : 4        
        if mod(i, 2) == 0
            start_state(i, 1) = 1;
        end
    end
    
    for i = 0:15
        state = start_state;
        X = de2bi(i);
        for j = 1:size(X, 2)
            if X(1, j) == 1
                state(j, 1) = 0.5;
            end
        end
        all_states(:, i+1) = state;
    end
    actions = [];
    allQvalues = [];
    for i = 1:size(all_states, 2)
        [q_value, bestAction, q_values] = getMaxQ(all_states(:, i));
        actions = [actions bestAction];        
        allQvalues = [allQvalues q_value];
        if i == 1
            display(q_values)
        end
    end
    display([actions; allQvalues]);
end

function log(message)
    global agent_vars;
    fprintf(1, message);
    if agent_vars.traceFileID > 0
        fprintf(agent_vars.traceFileID, message);
        
    end
end

function my_cleanup()
    global agent_vars;
    %display(agent_vars.return)
    %display(agent_vars.accuracy)
    agent_vars=rmfield(agent_vars,'gamma');
    agent_vars=rmfield(agent_vars,'epsilon');
    agent_vars=rmfield(agent_vars,'stepsize');

    clear agent_vars;

end

function alpha = get_alpha()
    alpha = 0.9;
end

function policyTree = getTree()
    global agent_vars;
    % Initialize the state
    agent_vars.currState = zeros(2*agent_vars.noFeatures, 1);
    %fprintf(1, 'Number of Features %d\n', agent_vars.noFeatures);
    for i = 1 : size(agent_vars.currState, 1)
        %TODO: Is this correct - ? Will yeild an encoding 0 1 0 1 ..... 0 1
        %right?
        if mod(i, 2) == 0
            agent_vars.currState(i, 1) = 1;
        end
    end    
end